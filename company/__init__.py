from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database/db.db"
app.config["SECRET_KEY"] = "'17ab226de066a0f995895791b9e726fbb14042df6583763d1df5084dfc3b53e8'"
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.login_message  = "برای دسترسی به این صفحه ابتدا باید وارد شوید."
login_manager.login_message_category   = "info"
from company.routes import *