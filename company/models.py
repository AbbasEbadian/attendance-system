from datetime import datetime, time
from company import db, login_manager
from flask_login import UserMixin
import jdatetime
DATE_FORMAT = "%Y/%m/%d"
TIME_FORMAT = "%H:%M"
DATETIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT

@login_manager.user_loader
def load_user(user_id):
    return Employee.query.get(int(user_id))

class Employee(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    username = db.Column(db.String(20), nullable=False)
    password = db.Column(db.String(32), nullable=False)
    requests = db.relationship('OffRequest', backref="employee")
    reports = db.relationship('DayReport', backref="employee")
    create_date = db.Column(db.DateTime, default=datetime.utcnow)

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id': self.id,
           'name': self.name,
           'requests': [x.serialize for x in sorted(self.requests, key=lambda f:f.date, reverse=True)],
           'reports': [x.serialize for x in sorted(self.reports, key=lambda f:f.date, reverse=True)],  
       }
    @property
    def is_admin(self):
        return self.id == 1

    def __repr__(self):
        return f"{self.name}"


class OffRequest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer, db.ForeignKey("employee.id"))
    date = db.Column(db.DateTime())
    date_end = db.Column(db.DateTime())
    status = db.Column(db.String(200), default="unseen")
    type_id = db.Column(db.Integer, db.ForeignKey('off_type.id'))
    type = db.relationship('OffType',
        backref=db.backref('requests', lazy=True))
    obj_id = db.Column(db.Integer, db.ForeignKey('obj_type.id'))
    obj = db.relationship('ObjType',
        backref=db.backref('requests', lazy=True))
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id': self.id,
           'date': self.date.strftime(DATETIME_FORMAT),
           'employee_id': self.employee_id,
           'status': self.status,
       }
    def jalali_date_from(self, time=False):
        format = time and DATETIME_FORMAT or DATE_FORMAT
        date = jdatetime.datetime.fromgregorian(datetime=self.date).strftime(format)
        return date

    def jalali_date_to(self, time=False):
        format = time and DATETIME_FORMAT or DATE_FORMAT
        date = jdatetime.datetime.fromgregorian(datetime=self.date_end).strftime(format)
        return date




    def __repr__(self):
        return f"{self.employee} - {self.request_type} - {self.date}"


class OffType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    initial = db.Column(db.Integer, default=0)
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id': self.id,
           'name': self.name,
           'initial': self.initial
       }

    def __repr__(self):
        return f"{self.name}"

# Objective Type
class ObjType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    initial = db.Column(db.Integer, default=0)
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id': self.id,
           'name': self.name,
           'initial': self.initial
       }

    def __repr__(self):
        return f"{self.name}"

class DayReport(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date())
    checkin = db.Column(db.Time())
    checkout = db.Column(db.Time())
    objective = db.Column(db.String(200))
    employee_id = db.Column(db.Integer, db.ForeignKey("employee.id"))
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id': self.id,
           'date': self.date and self.date.strftime(DATE_FORMAT) ,
           'checkin': self.checkin and self.checkin.strftime(TIME_FORMAT),
           'checkout': self.checkout and  self.checkout.strftime(TIME_FORMAT),
           'objective':  self.objective and  self.objective,
           'employee_id': self.employee_id and self.employee_id,
           'jdate': self.jalali_date()

       }

    def jalali_date(self, time=False):
        format = time and DATETIME_FORMAT or DATE_FORMAT
        date = jdatetime.datetime.fromgregorian(datetime=self.date).strftime(format)
        return date

    def __repr__(self):
        return f"{self.employee} - {self.date} - {self.checkin} To {self.checkout}"
