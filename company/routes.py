from wtforms.form import FormMeta
from company import app
from flask import render_template, request, flash, redirect, url_for
import json
from company.models import Employee, OffRequest, DayReport, OffType, ObjType
from company.forms import RegisterForm, LoginForm, RequestForm
from sqlalchemy import desc
from company import db, app, bcrypt
from flask_login import login_user, current_user, logout_user, login_required
from datetime import datetime, timedelta
from datetime import date as ddate

@app.route("/")
def home():
    if current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('user'))
    else:
        return redirect(url_for('login'))


@app.route('/admin', methods=["POST", "GET"])
@login_required
def admin():
    if not current_user.is_admin:
        return redirect(url_for('user'))
    users = Employee.query.filter(Employee.id!=1)
    users = [user.serialize for user in users]
    for i in range(len(users)):
        users[i]["last_report"] = users[i]['reports'] and users[i]["reports"][0] or None

    offtypes = [x.serialize for x in OffType.query.all() ]
    objtypes = [x.serialize for x in ObjType.query.all()]
    offrequests =  sorted(OffRequest.query.filter(OffRequest.status!='cancelled'), key=lambda x: x.date, reverse=True)
    return render_template("admin.html", 
        users=users, offtypes=offtypes,offrequests=offrequests,objtypes=objtypes
        )

@app.route('/user', methods=["POST", "GET"])
@login_required
def user():
    if current_user.is_admin:
        return redirect(url_for('admin'))

    form = RequestForm()
    return render_template("user.html",
     users=[], offtypes=[],offrequests=[],form=form
     )


@app.route('/get_reports', methods=["POST"])
def get_reports():
    emp_id = request.form.get("employee_id", None)
    if not emp_id:
        return {"result": "fail", "cause": "no employee name provided"}
    emp = Employee.query.filter_by(id=emp_id).first()
    reports = emp.serialize["reports"]
    return json.dumps({"reports": reports})


@app.route('/create_dayreport', methods=["POST"])
def create_day_report():
    return json.dumps({"test": 1})


@app.route('/create_request', methods=["POST"])
def create_request():
    try:
        form = RequestForm()
        if form.validate_on_submit():
            type = request.form.get("request_type")
            type_id = request.form.get(type+"_type")
            date = request.form.get("date", 0)
            date_end = request.form.get("date_end", 0)
            date_start_unix = request.form.get("date_start_unix", 0)
            date_end_unix = request.form.get("date_end_unix", 0)
            date_start = datetime.fromtimestamp(int(date_start_unix)//1000)
            date_end = datetime.fromtimestamp(int(date_end_unix)//1000)
            req =  {
                "employee":current_user,
                "date": date_start,
                "date_end": date_end,
                "status": "unseen" 
            }
            if type == "off":
                type = OffType.query.get(int(type_id))
                req["type"] = type
            else:
                type = ObjType.query.get(int(type_id))
                req["obj"] = type
            new_req = OffRequest(**req)
            db.session.add(new_req)
            db.session.commit()
            return json.dumps({"result": "success", "id":new_req.id})
        else:
            return json.dumps({"result": "warning", "form":form.errors})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})



@app.route('/add_off_type', methods=["POST"])
def add_off_type():
    name = request.form.get("name", None)
    initial = request.form.get("initial", 0)
    if not name :
        return json.dumps({"result": "fail", "cause":"name attribute was not provided"})

    new_type = OffType(name=name, initial=initial)
    db.session.add(new_type)
    db.session.commit()
    return json.dumps({"result": "success", "id":new_type.id})

@app.route('/delete_off_type', methods=["POST"])
def delete_off_type():
    try:
        _id = request.form.get("id", None)
        _type = OffType.query.filter_by(id=int(_id)).first()
        db.session.delete(_type)
        db.session.commit()
        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})

@app.route('/add_obj_type', methods=["POST"])
def add_obj_type():
    name = request.form.get("name", None)
    initial = request.form.get("initial", 0)
    if not name :
        return json.dumps({"result": "fail", "cause":"name attribute was not provided"})

    new_type = ObjType(name=name, initial=initial)
    db.session.add(new_type)
    db.session.commit()
    return json.dumps({"result": "success", "id":new_type.id})

@app.route('/delete_obj_type', methods=["POST"])
def delete_obj_type():
    try:
        _id = request.form.get("id", None)
        _type = ObjType.query.filter_by(id=int(_id)).first()
        db.session.delete(_type)
        db.session.commit()
        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})


@app.route('/confirm_request_line', methods=["POST"])
def confirm_request_line():
    try:
        _id = request.form.get("id", None)
        _request = OffRequest.query.filter_by(id=int(_id)).first()
        _request.status = "accepted"
        db.session.commit()
        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})


@app.route('/reject_request_line', methods=["POST"])
def reject_request_line():
    try:
        _id = request.form.get("id", None)
        _request = OffRequest.query.get(id=int(_id)).first()
        if _request:
            if _request.status == "unseen":
                _request.status = "rejected"
                db.session.commit()
            else:
                return json.dumps({"result": "fail", "cause": "این درخواست قبلا پاسخ داده شده است.لطفا صفحه را از نو بارگذاری نمایید."})
        else:
            return json.dumps({"result": "fail", "cause": "درخواست یافت نشد."})

        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})
        

@app.route('/cancel_request_line', methods=["POST"])
def cancel_request_line():
    try:
        _id = request.form.get("id", None)
        _request = OffRequest.query.filter_by(id=int(_id)).first()
        if _request:
            if _request.status == "unseen":
                _request.status = "cancelled"
                db.session.commit()
            else:
                return json.dumps({"result": "fail", "cause": "این درخواست قبلا پاسخ داده شده است.لطفا صفحه را از نو بارگذاری نمایید."})
        else:
            return json.dumps({"result": "fail", "cause": "درخواست یافت نشد."})

        db.session.commit()
        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})
        

@app.route('/register', methods=["POST", "GET"])
def register():
    if current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(url_for('admin'))
        return redirect(url_for("user"))
    form = RegisterForm()
    if form.validate_on_submit():
        hash_pwd = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = Employee(name=form.name.data, username=form.username.data, password=hash_pwd)
        db.session.add(user)
        db.session.commit()
        flash("حساب کاربری با موفقیت ایجاد شد.", 'success')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)
        

@app.route('/login', methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(url_for('admin'))
        return redirect(url_for("user"))
    form = LoginForm()
    if form.validate_on_submit():
        user = Employee.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            if current_user.is_admin:
                return redirect(url_for('admin'))
            return redirect(url_for("user"))
        else:
            flash("نام کاربری یا رمز عبور اشتباه است", 'danger')
    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect( url_for("login") )


@app.route('/get_custom_report', methods=["POST"])
def get_custom_report():
    start_date = request.form.get("start_date", 0)
    end_date = request.form.get("end_date", 0)
    start_date = datetime.fromtimestamp(int(start_date)//1000)
    end_date = datetime.fromtimestamp(int(end_date)//1000)
    requests = OffRequest.query.filter(OffRequest.employee==current_user, OffRequest.date >= start_date, OffRequest.date <= end_date, OffRequest.status=='accepted')
    off_hours = 0
    off_days = 0
    obj_hours = 0
    obj_days = 0
    for req in requests:
        diff = (req.date_end - req.date)
        if req.type:
            off_hours += abs(diff.total_seconds()/3600)
            off_days += abs(diff.days)
        else:
            obj_hours += abs(diff.total_seconds()/3600)
            obj_days += abs(diff.days)
    reports = DayReport.query.filter(DayReport.employee==current_user, OffRequest.date >= start_date, OffRequest.date <= end_date)
    total_time = 0
    for report in reports:
        if report.checkout and report.checkin:
            a = datetime.combine(ddate.min, report.checkout)
            b = datetime.combine(ddate.min, report.checkin)
            total_time += abs(a - b).total_seconds()/3600
    print(total_time * 60)
    return json.dumps({
        "result": "success",
        "data":{
            "total_time": round(total_time),
            "off_hours": round(off_hours)%24, 
            "off_days": round(off_days), 
            "obj_hours": round(obj_hours)%24, 
            "obj_days": round(obj_days), 
            }
        })
    
@app.route("/submit_checkin", methods=["post"])
def submit_checkin():
    try:
        current_time = datetime.now().time()
        current_date = datetime.now().date()
        last_report = DayReport.query.filter(
            DayReport.employee==current_user,
            DayReport.checkin != None,
            DayReport.checkout == None
        ).first()
        if last_report:
            return json.dumps({"result": "fail", "cause":"زمان خروج روز قبل ثبت نشده است."})
        latest_report = DayReport.query.filter(
            DayReport.employee==current_user,
        ).order_by(DayReport.id.desc()).first()
        last_date = latest_report and latest_report.date + timedelta(days=1) or current_date
        last_report = DayReport(employee=current_user, checkin=current_time, date=last_date)
        db.session.add(last_report)
        db.session.commit()
        return json.dumps({"result": "success"})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})

@app.route("/submit_checkout", methods=["post"])
def submit_checkout():
    try:
        current_time = datetime.now().time()
        last_report = DayReport.query.filter(
            DayReport.employee==current_user,
            DayReport.checkin != None,
            DayReport.checkout == None
        ).first()
        if  last_report:
            last_report.checkout = current_time
            db.session.commit()
            return json.dumps({"result": "success"})
        else:
            return json.dumps({"result": "fail", "cause":"زمان خروج قبلا ثبت شده است."})
    except Exception as e:
        return json.dumps({"result": "fail", "cause":str(e)})