Array.prototype.last = (array)=>{
    if (array.length == 0) return none;
    return array[array.length-1];
}
toastr.options = {
    "closeButton": true,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
  
function reload(delay=0){
    setTimeout(() => {
        document.location.reload();
    }, delay);
}
$(function(ready){
    /* Sidebar menu click listener */

    $(".date_start input, .date_end input, #date_start_unix, #date_end_unix").val("");

    $(".sidebar a").off().click((e)=>{
        let target = $(e.target).data("target");
        $(e.target).addClass("active");
        $(e.target).parent().siblings().find("a").removeClass("active");
        $(".admin_dashboard .content article").addClass('d-none');
        $(`.admin_dashboard .content article#${target}`).removeClass('d-none');
    });

    if (document.location.hash){
        $(`.sidebar a[href="${document.location.hash}"]`).click();
    }
    /* Report section employee selector listener */
    $("#reports select").off().change((e)=>{
        let employee_id = $("#reports select :selected").val();
        console.log(this, employee_id);
        let post = $.post({
            url: '/get_reports',
            data: {employee_id : employee_id},
        });
        post.done((data)=>{
            data = JSON.parse(data)["reports"];
            const table = $('#reports table');
            if(data.length > 0){
                table.removeClass("d-none");
                table.find("tbody tr").detach();
                $.each(data, (i, item)=>{
                    let tr = $(
                        `<tr>
                            <th scope='row'> ${i+1}</th>
                            <td>${item["jdate"] || '-'}</td>
                            <td>${item["checkin"] || '-'}</td>
                            <td>${item["checkout"] || '-'}</td>
                        </tr>`
                        );
                    table.find('tbody').append(tr);
                });
            }else{
                table.addClass("d-none");
            }
        });
    });


    /* Add new Type */
    $("div.type_element.add").click((e)=>{
        let type = "absence_types";
        if ($(e.target).parents('.absence_types').length==0)
            type = "objective_types";
        $("." + type).find("div.type_element.new").removeClass("d-none");
    });
    $("div.type_element.new button").click((e)=>{
        const butt = $(e.target)
        const inp = butt.parents("div.new").find('input');
        const url = butt.parents(".types_row").data("create_url");
        butt.parents('.new').addClass("d-none");
        if(inp.val().length > 0){
            const text = inp.val();
            $.post({
                url: "/"+url,
                data: {name: text, initial:0},
            }).done((result)=>{
                result = JSON.parse(result);
                if(result["result"] == "success"){
                    let row = $(`
                        <div class="type_element" data-id="${result.id}"}} data-closable="0">
                            <strong>${text}</strong>
                            <button type="button" class="close btn-simple"  aria-label="Close" >
                                <i class="bi-trash"></i>
                            </button>
                        </div>
                    `);
                    row.insertBefore(butt.parents('.types_row').find(".type_element.add"));
                    toastr.success("با موفقیت ایجاد شد.");
                    inp.val("");
                }else{
                    toastr.error(result['cause']);
                }
            }).always(()=>{
                butt.parents('.new').addClass("d-none");
            });
        }
    });

    $("div.type_element button.close").off().click((e)=>{
        const butt = $(e.target)
        const id = butt.parent().data('id');
        const url = butt.parents(".types_row").data("delete_url");

        $.post({
            url: '/'+url,
            data: {id: id}
        }).done((data)=>{
            data = JSON.parse(data);
            if (data.result == 'success') {
                toastr["success"]("با موفقیت حذف شد.");
                $(e.target).parents('.type_element').detach();
            }
            else
                toastr.success("مشکلی هنگام حذف کردن پیش آمد.");
        });
    });

    
    $(".admin_dashboard .request_line button.reject").off().click((e)=>{
        const butt = $(e.target);
        const id = butt.parents('.request_line').data('id');
        $.post({
            url: '/reject_request_line',
            data: {id: id}
        }).done((data)=>{
            data = JSON.parse(data);
            if (data.result == 'success') {
                toastr["success"]("با موفقیت رد شد.");
                document.location.reload();
            } else
                toastr.success("مشکلی هنگام رد درخواست پیش آمد.");
        });
    });

    $('select#request_type').change(()=>{
        const current = $("select#request_type :selected").val();
        $(".type_selector").addClass("d-none");
        $(".type_selector." + current).removeClass("d-none");
    });
    $('select#request_type').change();

    $(".create_new_request").click(()=>{
        $("fieldset.new_request_form").removeClass("d-none");
    });
    $("form.create_request .cancel").click(()=>{
        $("fieldset.new_request_form").addClass("d-none");
    });
    $(".cancel_request").click((e)=>{
        const id = $(e.target).parents('.request').data('id');
        const url = "cancel_request_line";
        console.log(id);
        if (!id) return;
        $.post({
            url: url,
            data: {id: id},
            
        }).done((data)=>{
            data= JSON.parse(data);
            if (data.result == 'success'){
                toastr.success("درخواست شما با لغو  شد.");
                setTimeout(()=>{
                    document.location.reload();
                }, 2);
            }else{
                toastr.error("درحال حاضر امکان لغو درخواست وجود ندارد.")
                console.log(result.cause);
            }
        })
    });
    let submited = false;
    $('form.create_request').off().submit(function (e) {
        const form = $(this);
        e.preventDefault(); // block the traditional submission of the form.
        if (submited) return;
        var url = "create_request"; // send the form data here.
        form.find('input').removeClass('is-invalid');
        form.find('.invalid-feedback').addClass('d-none');
        $.post({
            url: url,
            data: $('form.create_request').serialize(), 
            
        }).done((data)=>{
            data= JSON.parse(data);
            if (data.result == 'success'){
                submited = true;
                toastr.success("درخواست شما با موفقیت ثبت شد.");
                document.location.reload();
            } else if(data.result == "warning"){
                $.each(Object.keys(data.form), (i, key)=>{
                    const cont = $(`[data-error_class=${key}]`);
                    cont.find('input').addClass('is-invalid');
                    cont.find('.invalid-feedback').removeClass('d-none');
                    cont.find('span.error').text(data.form[key][0])
                });
            }else{
                toastr.error("درحال حاضر امکان ثبت درخواست وجود ندارد.")
                console.log(data.result.cause);
            }
        })
        
    });

    let to = $("fieldset.new_request_form input#date_end").persianDatepicker({
        format:"L HH:mm dddd",
        autoClose:false,
        initialValue:false,
        toolbox:{
            submitButton: {
                enabled: true,
                onSubmit: ()=>{
                    let unix = to.getState().selected.unixDate;
                    $("form.create_request input#date_end_unix").val(unix); 
                    to.hide();
                }
            },
            calendarSwitch: {
                enabled: false
            }
        },
       
        navigator:{
            scroll:{
                enabled: false
            }
        },
        minDate: new persianDate(),
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            },
            second: {
                enabled: false
            }
        },
        onSelect: function (unix) {
            to.touched = true;
            if (from && from.options && from.options.maxDate != unix) {
                var cachedValue = from.getState().selected.unixDate;
                from.options = {maxDate: unix};
                if (from.touched) {
                    from.setDate(cachedValue);
                }
            }
            

        }
    });
    let from = $("fieldset.new_request_form input#date").persianDatepicker({
        format:"L HH:mm dddd",
        initialValue:false,
        autoClose:false,
        toolbox: false,
        toolbox:{
            submitButton: {
                enabled: true,
                onSubmit: ()=>{
                    let unix = from.getState().selected.unixDate;
                    $("form.create_request input#date_start_unix").val(unix); 
                    from.hide();
                    $("form.create_request input#date_end").focus();
                }
            },
            calendarSwitch: {
                enabled: false
            }
        },
        navigator:{
            scroll:{
                enabled: false
            }
        },
        minDate: new persianDate(),
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            },
            second: {
                enabled: false
            }
        },
        onSelect: function (unix) {
            from.touched = true;
            if (to && to.options && to.options.minDate != unix) {
                var cachedValue = to.getState().selected.unixDate;
                to.options = {minDate: unix};
                if (to.touched) {
                    to.setDate(cachedValue);
                }
            }
            
        }
        
    });
    

    let to1 = $("#reports input#report_date_end").persianDatepicker({
        format:"L HH:mm dddd",
        autoClose:false,
        initialValue:false,
        toolbox:{
            enabled: false
        },
       
        navigator:{
            scroll:{
                enabled: false
            }
        },
        minDate: new persianDate(),
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            },
            second: {
                enabled: false
            }
        },
        onSelect: function (unix) {
            to1.touched = true;
            if (from1 && from1.options && from1.options.maxDate != unix) {
                var cachedValue = from1.getState().selected.unixDate;
                from1.options = {maxDate: unix};
                if (from1.touched) {
                    from1.setDate(cachedValue);
                }
            }
            $("#reports input#date_end_unix").val(unix); 
            to1.toggle();

        }
    });
    let from1 = $("#reports input#report_date_start").persianDatepicker({
        format:"L HH:mm dddd",
        initialValue:false,
        autoClose:false,
        toolbox: false,
        toolbox:{
            enabled: false
        },
        navigator:{
            scroll:{
                enabled: false
            }
        },
        minDate: new persianDate(),
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            },
            second: {
                enabled: false
            }
        },
        onSelect: function (unix) {
            from1.touched = true;
            if (to1 && to1.options && to1.options.minDate != unix) {
                var cachedValue = to1.getState().selected.unixDate;
                to1.options = {minDate: unix};
                if (to1.touched) {
                    to1.setDate(cachedValue);
                }
                to1.toggle();
            }
            $("#reports input#date_start_unix").val(unix); 
            from1.toggle();
            $("#reports input#report_date_end").focus();
        }
        
    });
    $("#reports button.get_info").click((e)=>{
        const butt = $(e.target);
        const unix_start = $("#reports").find("#date_start_unix").val();
        const unix_end   = $("#reports").find("#date_end_unix").val();
        $.post({
            url: "get_custom_report",
            data: {start_date: unix_start, end_date: unix_end}
        }).done((data)=>{
            data= JSON.parse(data);
            if (data.result == 'success') {
                const table = $("#reports table");
                $.each(Object.keys(data.data), (i, key)=>{
                    table.find("."+key).text(data.data[key]);
                });
            }else
                toastr.error("دریافت اطلاعات فعلا مقدور نمی باشد.")
        });
    });
    $("button.submit_checkin").click(function (e) {
        $.post({
            url: "submit_checkin",
            data: {}
        }).done((data)=>{
            data= JSON.parse(data);
            if (data.result == 'success') {
                document.location.reload();
            }else{
                toastr.error(data.cause)
            }
        });
    });
    $("button.submit_checkout").click(function (e) {
        $.post({
            url: "submit_checkout",
            data: {}
        }).done((data)=>{
            data= JSON.parse(data);
            if (data.result == 'success') {
                document.location.reload();
            }else{
                toastr.error(data.cause)
            }
        });
    });
    $(".admin_dashboard .request_line button.confirm").off().click((e)=>{
        const butt = $(e.target);
        const id = butt.parents('.request_line').data('id');
        $.post({
            url: '/confirm_request_line',
            data: {id: id}
        }).done((data)=>{
            data = JSON.parse(data);
            if (data.result == 'success') {
                toastr["success"]("با موفقیت تایید شد.");
                console.log("came");
                document.location.reload();
            }else{
                toastr.error("مشکلی هنگام تایید درخواست پیش آمد.");
            }
        });
    });
});