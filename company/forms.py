from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField, SubmitField, BooleanField
from wtforms import  SelectField,HiddenField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, length, EqualTo, ValidationError
from company.models import Employee, OffType, ObjType


length_message = "حداقل 5 و حداکثر 20 کاراکتر"
empty_message = "نمی تواند خالی باشد"
match_message = "رمز عبور های وارد شده یکسان نیستند"
class RegisterForm(FlaskForm):
    name = StringField('نام و نام خانوادگی', validators=[DataRequired(message=empty_message), length(min=5, max=20, message=length_message)])
    username = StringField('نام کاربری', validators=[DataRequired(message=empty_message), length(min=5, max=20, message=length_message)])
    password = PasswordField('رمز عبور', validators=[DataRequired(message=empty_message)])
    confirm_password = PasswordField('تکرار رمز عبور', validators=[DataRequired(message=empty_message), EqualTo('password', message=match_message)])

    submit = SubmitField('ثبت نام')

    def validate_username(self, username):
        user = Employee.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError("این نام کاربری قبلا ثبت شده است.")
        

class LoginForm(FlaskForm):
    username = StringField('نام کاربری', validators=[DataRequired(message=empty_message), length(min=5, max=20, message=length_message)])
    password = PasswordField('رمز عبور', validators=[DataRequired(message=empty_message)])
    remember = BooleanField('مرا به خاطر بسپار')
    submit = SubmitField('ورود')

def get_off():
    return OffType.query.all()
def get_obj():
    return ObjType.query.all()
class RequestForm(FlaskForm):
    
    request_type = SelectField('نوع درخواست', choices=[("off", "مرخصی"), ("obj", "ماموریت")])
    off_type = QuerySelectField('نوع مرخصی', query_factory=get_off,
                            get_pk=lambda a: a.id,
                            get_label=lambda a: a.name)
    obj_type = QuerySelectField('نوع ماموریت', query_factory=get_obj,
                            get_pk=lambda a: a.id,
                            get_label=lambda a: a.name)
    date = StringField('تاریخ شروع:', validators=[DataRequired(message='تاریخ شروع نمیتواند خالی باشد.')])
    date_end = StringField('تاریخ پایان:', validators=[DataRequired(message='تاریخ پایان نمیتواند خالی باشد.')])
    date_start_unix = HiddenField("", validators=[DataRequired(message='تاریخ شروع  نمیتواند خالی باشد.')])
    date_end_unix = HiddenField("", validators=[DataRequired(message='تاریخ پایان نمیتواند خالی باشد.')])
    
    submit = SubmitField('ارسال درخواست')